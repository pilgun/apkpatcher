from apkpatcher import *
from apkpatcher.cli import main
import logging
if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    main()
